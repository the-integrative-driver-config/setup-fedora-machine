# setup fedora machine
This project is for seting up, bootstrapping your fedora machine very quickly after install.

## Usage
The way this is intended to work is by using a curl and run the install script which bootstraps the configuration

``` bash
curl -s https://gitlab.com/the-integrative-driver-config/setup-fedora-machine/-/raw/main/run > run; bash run
```

## Development
For this I use just, a command runner.

``` bash
Available recipes:
    default       # Interactive menu to choose recipe
    docs          # Copy recipes docs
    lint          # Check linting on your programs
    list_packages # List packages to install
    run           # Run the configuration bootstrap script
    shell         # Run a shell inside your container
    test          # Test the configuration bootstrap script
```
