# Interactive menu to choose recipe
default:
  @just --choose

# Run the configuration bootstrap script
run:
  #!/bin/bash
  ./run

# Test the configuration bootstrap script
test:
  #!/bin/bash
  IMAGE="myfedora:38"
  docker build -t $IMAGE . 
  docker run -it $IMAGE ./test 

# Run a shell inside your container
shell:
  #!/bin/bash
  IMAGE="myfedora:38"
  docker build -t $IMAGE . 
  docker run -it $IMAGE bash 

# Check linting on your programs
lint:
  #!/bin/bash
  shellcheck run 
  glab ci lint

# List packages to install
list_packages:
  #!/bin/bash
  python3 scripts/list_packages.py "flatpak,dnf" 

# Copy recipes docs
docs:
  #!/bin/bash
  just -l | xclip -sel clip
  nvim README.md +/Available recipes:

# List installed rust packages
cargo:
  #!/bin/bash
  cargo install --list

# List installed dnf packages
dnf:
  dnf repoquery --userinstalled --qf "%{name}"

# List installed flatpak packages
flatpak:
  flatpak list --columns=application
