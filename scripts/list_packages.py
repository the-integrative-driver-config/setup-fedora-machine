import subprocess
import yaml
import typer

app = typer.Typer()

def package_manager(name: str) -> None:
    manager = {
        "dnf": ["dnf", "repoquery", "--userinstalled", "--qf", "%{name}"],
        "flatpak": ["flatpak", "list", "--columns=application"]
    }
    result = set(subprocess.run(
        manager[name], 
        capture_output=True, 
        text=True, 
        check=True
    ).stdout.strip().split("\n"))
    with open(f'ansible/vars/ignore_packages.yml', 'r') as yaml_file:
        ignore_packages = yaml.safe_load(yaml_file)
        ignore_content = set(ignore_packages["ignore_packages"])
    with open(f'ansible/vars/ignore_packages.yml', 'w') as yaml_file:    
        sorted_packages = sorted(list(ignore_content))
        print(sorted_packages)
        yaml.dump({"ignore_packages": sorted_packages}, yaml_file)
    with open(f'ansible/vars/{name}_packages.yml', 'w') as yaml_file:    
        packages = {
            f"{name}_packages": sorted(list(result - ignore_content))
        }
        yaml.dump(packages, yaml_file)
    print(f"Added packages to {name}_packages.yml") 

@app.command()
def managers(names: str):
    """
    Names should be comma seperated: dnf, flatpak
    """
    for name in names.split(','):
        match name:
            case "dnf":
                package_manager(name) 
            case "flatpak":
                package_manager(name) 
            case _:
                print(f"{name} is not an option")

if __name__ == "__main__":
  app()
